/* eslint-disable no-alert */
import Vue from 'vue';
import App from './App.vue';

import { helloMessage, byeMessage } from './data';
import showMessage from './functions';

Vue.config.productionTip = false;

new Vue({
  render: (h) => h(App),
}).$mount('#app');

showMessage(helloMessage);
showMessage(byeMessage);
